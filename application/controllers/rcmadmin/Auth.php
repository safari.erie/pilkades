<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	var $url   = 'auth';
	var $model = 'Model_auth';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_auth');
	}


	public function index()
	{

		$asset = array(
					'title' => $this->lang->line('title_auth')
				 );

		$post = $this->input->post();
		if ( $post ) {
			$data = $this->{$this->model}->login($post['username'], $post['password']);
			if ( $data ) {
				$this->session->set_userdata('id', $data['id']);
				$this->session->set_userdata('nama', $data['nama']);
				$this->session->set_userdata('username', $data['username']);
				$this->session->set_userdata('role', $data['role']);
				$this->session->set_userdata('kec_id', $data['kec_id']);
				$this->session->set_userdata('desa_id', $data['desa_id']);
				$this->session->set_userdata('nama_kecamatan', $data['nama_kecamatan']);
				$this->session->set_userdata('nama_desa', $data['nama_desa']);

				redirect('rcmadmin/home');
				
			} else {
				$this->session->set_flashdata('warning', 'Username/Password Salah');
			}
		}


		$this->load->view('rcmadmin/template/login/header', $asset);		
		$this->load->view('rcmadmin/' . $this->url . '/index');
		$this->load->view('rcmadmin/template/login/footer', $asset);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/rcmadmin/auth');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/rcmadmin/Auth.php */