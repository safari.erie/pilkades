<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tungsura extends CI_Controller {

	var $url   				= 'tungsura';
	var $model 				= 'Model_tungsura';
	var $model_tps   		= 'Model_tps';
	var $model_kecamatan	= 'Model_kecamatan';

	public function __construct()
	{
		parent::__construct();
		check_login();
		$this->load->model('Model_tungsura');
		$this->load->model('Model_tps');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "Master Data > List " . $this->url
		);
		
        $data = array(
            "list_tungsura" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "Master Data > Add " . $this->url
		);

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" => $this->{$this->model}->get_all_desa_by_user(),
		);
		
		$post = $this->input->post();
		if ( isset($post['save']) ) {
			$post['created_by'] = $this->session->userdata('id');

			$paramsTps = array(
				"tungsura_tidak_sah" => $post['tungsura_tidak_sah'],
			);
			$this->{$this->model}->update('tps', $paramsTps, array('id' => $post['tps_id']));

			foreach ($post['jumlah_suara'] as $index => $value) {
				$params = array(
					"cakades_id"   => $index,
					"tps_id" 	   => $post['tps_id'],
					"jumlah_suara" => $value,
					"created_by"   => $post['created_by'],
				);

				$save = $this->{$this->model}->add($this->url, $params);
			}

			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function view()
	{
        $assets = array(
            "title_page" => "Master Data > View " . $this->url
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {
			$post['created_by'] = $this->session->userdata('id');
			unset($post['cakades_id']);
			unset($post['tps_id']);
			
			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"tungsura" => $this->{$this->model}->get_detail($id),
			"list_desa" => $this->{$this->model}->get_all_desa_by_user(),
		);

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function get_all_cakades_by_desa()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_cakades_by_desa($id);

		echo json_encode($data);
	}

	public function get_all_tps_by_desa()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_tps_by_desa($id);

		echo json_encode($data);
	}

	public function get_all_tps_by_desa_on_view()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_tps_by_desa_on_view($id);

		echo json_encode($data);
	}

	public function get_all_desa_by_kecamatan()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

}