<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	var $url   			 = 'user';
	var $model 			 = 'Model_user';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		check_login();
		$this->load->model('Model_user');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
		// cek role
		check_role();

        $assets = array(
            "title_page" => "Master Data > List " . $this->url
		);

		$role = array(
			"1" => "Administrator",
			"2" => "Petugas Kecamatan",
			"3" => "Petugas Desa",
		);
		
        $data = array(
			"list_user" => $this->{$this->model}->get_all(),
			"role" 		=> $role,
		);
		
		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function add()
	{
		// cek role
		check_role();

        $assets = array(
            "title_page" => "Master Data > Add " . $this->url
		);

		$role = array(
			"1" => "Administrator",
			"2" => "Petugas Kecamatan",
			"3" => "Petugas Desa",
		);

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" => $this->{$this->model_desa}->get_all(),
			"role" => $role,
		);
		
		$post = $this->input->post();
		$data['post'] = $post;
		if ( isset($post['save']) ) {
			unset($post['save']);
			$post['password'] = md5($post['password']);

			$save = $this->{$this->model}->add($this->url, $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function view()
	{
		// cek role
		check_role();
		
        $assets = array(
            "title_page" => "Master Data > View " . $this->url
		);

		$id = $this->uri->segment(4); 

		$role = array(
			"1" => "Administrator",
			"2" => "Petugas Kecamatan",
			"3" => "Petugas Desa",
		);
		
		$post = $this->input->post();
		if ( isset($post['save']) ) {
			unset($post['save']);
			$post['password'] = md5($post['password']);

			if ($post['role'] == 1) {
				$post['kec_id'] = 0;
				$post['desa_id'] = 0;
			}
			
			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		if ($post) {
			$post = $post;
			unset($post['password']);
		} else {
			$user = $this->{$this->model}->get_detail($id);
			if ($user) {
				$post['nama'] 	  = $user['nama'];
				$post['username'] = $user['username'];
				$post['role'] 	  = $user['role'];
				if ($post['role'] == 2) {
					$post['kec_id'] = $user['kec_id'];
				} elseif ($post['role'] == 3) {
					$post['desa_id'] = $user['desa_id'];
				}
			}

		}

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" => $this->{$this->model_desa}->get_all(),
			"role" => $role,
			"post" => $post,
		);


		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}