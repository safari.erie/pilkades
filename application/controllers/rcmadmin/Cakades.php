<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cakades extends CI_Controller {

	var $url   		= 'cakades';
	var $model 		= 'Model_cakades';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		check_login();
		$this->load->model('Model_desa');
		$this->load->model('Model_cakades');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "Master Data > List " . $this->url
		);
		
        $data = array(
            "list_cakades" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "Master Data > Add " . $this->url
		);

		$data = array(
			"list_desa" => $this->{$this->model_desa}->get_all(),
		);
		
		$post = $this->input->post();
		if ( $post ) {

			$params = array();
			foreach ( $post['desa_id'] as $index => $value ) {
				$params['desa_id'] = $value;
				$params['nama'] = $post['nama'][$index];
				$params['no_urut'] = $post['no_urut'][$index];
				$save = $this->{$this->model}->add($this->url, $params);
			}
			
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
	}
	
	public function view()
	{
        $assets = array(
            "title_page" => "Master Data > View " . $this->url
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"cakades" => $this->{$this->model}->get_detail($id),
			"list_desa" => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}