<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Home extends CI_Controller {

	var $url   = 'home';
	var $model = 'Model_home';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_home');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
	}

	public function index()
	{
		check_login();
		$assets = array();

		$post = $this->input->post();
		if ( $this->session->userdata('role') == 2 ) {
			$post['kec_id'] = $this->session->userdata('kec_id');
			$post['kec_id_disabled'] = "disabled";
		} elseif ( $this->session->userdata('role') == 3 ) {
			$data = $this->{$this->model_desa}->get_detail($this->session->userdata('desa_id'));
			$post['kec_id']  = $data['kec_id'];
			$post['desa_id'] = $this->session->userdata('desa_id');
			$post['kec_id_disabled'] = "disabled";
			$post['desa_id_disabled'] = "disabled";
		}

		if ( $post ) {
			if (isset($post['reset']) ) {
				redirect('rcmadmin/home');
			}

			if ( isset($post['export']) ) {
				$this->export_data( $post );
			}
			unset($post['filter']);
			unset($post['reset']);
		}

		$data = array(
			"resp" => $this->{$this->model}->get_all($post),
			"list_kecamatan" => $this->{$this->model}->get_all_kecamatan($post),
			"post" => $post,
		);

		$this->load->view('rcmadmin/template/home/header', $assets);		
		$this->load->view('rcmadmin/template/home/menu');		
		$this->load->view('rcmadmin/' . $this->url . '/index', $data);	
		$this->load->view('rcmadmin/template/home/footer', $assets);	
	}

	public function get_chart_by_desa()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_chart_by_desa($id);

		echo json_encode($data);
	}

	public function get_chart_by_tps()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_chart_by_tps($id);

		echo json_encode($data);
	}

	public function get_all_desa_by_kecamatan()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

	public function export_data( $post ) {
		$alphas = range("A", "Z");

		$index = 25;
		foreach(range('A', 'Z') as $letter1) {
			foreach(range('A', 'Z') as $letter2) {
				$index++;
				$alphas[$index] = $letter1 . $letter2;
			}
		}

    	$objPHPExcel = new PHPExcel();
		$sNAMESS = "Ryan Pramasyana";
		$objPHPExcel->setActiveSheetIndex(0);

		$resp = $this->{$this->model}->get_all($post);

		// pre($resp);
		$styleCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);

		$styleRight = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);

		$tableBorder = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000')
				)
			)
		);
		
		$no = 0;
		foreach ( $resp['data'] as $data ) {
			$no++;

			$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, "Kecamatan: " . $data['nama']);

			$noIndex = $no;
			foreach ($data['data_desa'] as $dataDesa) {
				$noIndex++;

				$objPHPExcel->getActiveSheet()->setCellValue("B" . $noIndex, "> Desa: " . $dataDesa['nama']);

				$objPHPExcel->getActiveSheet()->mergeCells('B' . ($noIndex + 1) . ':B' . ($noIndex + 5));
				$objPHPExcel->getActiveSheet()->setCellValue("B" . ($noIndex + 1), "No Urut");

				$objPHPExcel->getActiveSheet()->mergeCells('C' . ($noIndex + 1) . ':C' . ($noIndex + 5));
				$objPHPExcel->getActiveSheet()->setCellValue("C" . ($noIndex + 1), "Calon Kepala Desa");

				$totalDataTps = count($dataDesa['data_tps']);

				$objPHPExcel->getActiveSheet()->mergeCells('D' . ($noIndex + 1) . ':'. $alphas[3 + ($totalDataTps - 1)] . ($noIndex + 1));
				$objPHPExcel->getActiveSheet()->setCellValue("D" . ($noIndex + 1), "Jumlah Suara");

				$totalHakPilih = 0;
				$indexTps = 0;
				foreach ($dataDesa['data_tps'] as $dataTps) {
					$indexTps++;
					$totalHakPilih += $dataTps['hak_pilih'];
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[3 + ($indexTps - 1)] . ($noIndex + 2), $dataTps['nama']);
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[3 + ($indexTps - 1)] . ($noIndex + 3), "Hak Pilih: " . $dataTps['hak_pilih']);
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[3 + ($indexTps - 1)] . ($noIndex + 4), "Suara Tidak Sah: " . $dataTps['tungsura_tidak_sah']);
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[3 + ($indexTps - 1)] . ($noIndex + 5), "Suara Sah");
				}

				$objPHPExcel->getActiveSheet()->mergeCells($alphas[4 + ($totalDataTps -1)] . ($noIndex + 1) . ':' . $alphas[4 + ($totalDataTps -1)] . ($noIndex + 5));
				$objPHPExcel->getActiveSheet()->setCellValue($alphas[4 + ($totalDataTps -1)] . ($noIndex + 1), "Total Suara");	
				
				$totalKeseluruhan = 0;
				$indexCakades = 0;
				foreach ($dataDesa['data_cakades'] as $dataCakades) {
					$indexCakades++;
					$objPHPExcel->getActiveSheet()->setCellValue("B" . (($noIndex + 5) + $indexCakades), $dataCakades['no_urut']);
					$objPHPExcel->getActiveSheet()->setCellValue("C" . (($noIndex + 5) + $indexCakades), $dataCakades['nama']);

					$total = 0;
					$indexTps = 0;
					foreach ($dataCakades['data_tps'] as $dataTps) {
						$indexTps++;
						$total += $dataTps['jumlah_suara'];
						$objPHPExcel->getActiveSheet()->setCellValue($alphas[3 + ($indexTps - 1)] . ( ($noIndex + 5) + $indexCakades), $dataTps['jumlah_suara']);
					}

					$totalKeseluruhan += $total; 
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[4 + ($totalDataTps -1)] . (($noIndex + 5) + $indexCakades), $total);
				}

				$objPHPExcel->getActiveSheet()->getStyle('B' . (($noIndex + 6) + $indexCakades))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle($alphas[4 + ($totalDataTps - 1)] . ( ($noIndex + 6) + $indexCakades))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('B' . (($noIndex + 6) + $indexCakades))->applyFromArray($styleRight);
				$objPHPExcel->getActiveSheet()->mergeCells('B' . (($noIndex + 6) + $indexCakades) . ':'. $alphas[3 + ($totalDataTps - 1)] . ( ($noIndex + 6) + $indexCakades));
				$objPHPExcel->getActiveSheet()->setCellValue('B' . (($noIndex + 6) + $indexCakades), "Total Keseluruhan: ");
				$objPHPExcel->getActiveSheet()->setCellValue($alphas[4 + ($totalDataTps - 1)] . ( ($noIndex + 6) + $indexCakades), $totalKeseluruhan);

				$objPHPExcel->getActiveSheet()->getStyle('B' . (($noIndex + 7) + $indexCakades))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle($alphas[4 + ($totalDataTps - 1)] . ( ($noIndex + 7) + $indexCakades))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('B' . (($noIndex + 7) + $indexCakades))->applyFromArray($styleRight);
				$objPHPExcel->getActiveSheet()->mergeCells('B' . (($noIndex + 7) + $indexCakades) . ':'. $alphas[3 + ($totalDataTps - 1)] . ( ($noIndex + 7) + $indexCakades));
				$objPHPExcel->getActiveSheet()->setCellValue('B' . (($noIndex + 7) + $indexCakades), "Total Hak Pilih: ");
				$objPHPExcel->getActiveSheet()->setCellValue($alphas[4 + ($totalDataTps - 1)] . ( ($noIndex + 7) + $indexCakades), $totalHakPilih);

				$objPHPExcel->getActiveSheet()->getStyle("B" . ($noIndex + 1) . ":" . $alphas[4 + ($totalDataTps - 1)] . ( ($noIndex + 7) + $indexCakades))->applyFromArray($tableBorder);

				$noIndex = (($noIndex + 7) + $indexCakades) + 1;
			}
			$no = $noIndex;
		}

		$namaFile = "";
		if ( @$post['kec_id'] == "" && @$post['desa_id'] == "" ) {
			$namaFile = "EXPORT DATA HASIL PERHITUNGAN SUARA SELURUH KECAMATAN";
		} elseif ( @$post['kec_id'] != "" && @$post['desa_id'] == "" ) {
			$kec = get_kec_or_desa('kecamatan', $post['kec_id']);
			$namaFile = "EXPORT DATA HASIL PERHITUNGAN SUARA KECAMATAN " . $kec['nama'];
		} else {
			$desa = get_kec_or_desa('desa', $post['desa_id']);
			$namaFile = "EXPORT DATA HASIL PERHITUNGAN SUARA DESA " . $desa['nama'];
		}

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'. $namaFile .'.xls"'); 
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output'); 
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */