<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $url   = 'home';
	var $model = 'Model_home';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		// check_login();
		$this->load->model('Model_home');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
	}

	public function index()
	{
		$assets = array();

		$post = $this->input->post();
		if ( $post ) {
			if (isset($post['reset']) ) {
				redirect('home');
			}
			unset($post['filter']);
			unset($post['reset']);
		}

		$data = array(
			"resp" => $this->{$this->model}->get_all($post),
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"post" => $post,
		);

		$this->load->view('rcmadmin/template/blank/header', $assets);		
		$this->load->view($this->url . '/index', $data);	
		$this->load->view('rcmadmin/template/blank/footer', $assets);	
	}

	public function get_chart_by_desa()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_chart_by_desa($id);

		echo json_encode($data);
	}

	public function get_chart_by_tps()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_chart_by_tps($id);

		echo json_encode($data);
	}

	public function get_all_desa_by_kecamatan()
	{
		$id = urldecode($this->uri->segment(4));
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */