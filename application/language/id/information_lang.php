<?php

	//titles
	$lang['title_auth']                        = 'Login';
	$lang['title_registration']                = 'Registrasi';
	$lang['title_activation']                  = 'Aktivasi';
	$lang['title_reset_password']              = 'Ganti password';
	$lang['title_dashboard']                   = 'Dashboard';
	$lang['title_my_profile']                  = 'Profil';
	$lang['title_add_additional_feature']      = 'Tambah fitur tambahan';
	$lang['title_list_additional_feature']     = 'Daftar fitur tambahan';
	$lang['title_view_additional_feature']     = 'Lihat fitur tambahan';
	$lang['title_listing_booking']             = 'Daftar Pemesanan';
	$lang['title_listing_calendar']            = 'Daftar Kalender';
	$lang['title_listing_calendar_by_asset']   = 'Daftar kalender berdasarkan aset';
	$lang['title_closed']                      = 'Tutup';
	$lang['title_outside_transaction']         = 'Transaksi diluar';
	$lang['title_chat']                        = 'Percakapan';
	$lang['title_history_chat']                = 'Riwayat percakapan';
	$lang['title_add_discount']                = 'Tambah diskon';
	$lang['title_list_discount']               = 'Daftar diskon';
	$lang['title_view_discount']               = 'Lihat diskon';
	$lang['title_list_new_booking']            = 'Daftar pemesanan terbaru';
	$lang['title_list_transaction_history']    = 'Daftar riwayat transaksi';
	$lang['title_add_office_close']            = 'Tambah toko yang tutup';
	$lang['title_list_office_close']           = 'Daftar toko yang tutup';
	$lang['title_view_office_close']           = 'Lihat toko yang tutup';
	$lang['title_add_outside_transaction']     = 'Tambah transaksi diluar';
	$lang['title_list_outside_transaction']    = 'Daftar transaksi diluar';
	$lang['title_view_outside_transaction']    = 'Lihat transaksi diluar';
	$lang['title_add_user']                    = 'Tambah pengguna';
	$lang['title_list_user']                   = 'Daftar pengguna';
	$lang['title_view_user']                   = 'Lihat pengguna';
	$lang['title_add_policy']                  = 'Tambah kebijakan';
	$lang['title_list_policy']                 = 'Daftar kebijakan';
	$lang['title_view_policy']                 = 'Lihat kebijakan';
	$lang['title_rental_profile_address']      = 'Alamat';
	$lang['title_rental_profile_bank_account'] = 'Akun Bank';
	$lang['title_rental_profile_document']     = 'Dokumen';
	$lang['title_rental_profile']              = 'Profil Rental';
	$lang['title_my_profile']                  = 'Profil';
	$lang['title_add_service_area']            = 'Tambah area layanan';
	$lang['title_list_service_area']           = 'Daftar area layanan';
	$lang['title_view_service_area']           = 'Lihat area layanan';
	$lang['title_setting_asset_category']      = 'Atur kategori aset';
	$lang['title_detail_transaction_history']  = 'Lihat riwayat transaksi';
	$lang['title_list_transaction_history']    = 'Daftar riwayat transaksi';
	$lang['title_form_request_withdrawal']     = 'Formulir tarik dana';
	$lang['title_add_discount_longer_rent']    = 'Tambah harga diskon jangka panjang';
	$lang['title_add_price']                   = 'Tambah harga';
	$lang['title_view_price']                  = 'Lihat harga';
	$lang['title_asset']                       = 'Aset';
	$lang['title_listing_asset']               = 'Daftar aset';
	$lang['title_address']                     = 'Alamat';
	$lang['title_asset_detail']                = 'Detail aset';
	$lang['title_booking_rules']               = 'Aturan pemesanan';
	$lang['title_discount_longer_rent']        = 'Diskon jangka panjang';
	$lang['title_insurance']                   = 'Asuransi';
	$lang['title_photo']                       = 'Foto';
	$lang['title_price']                       = 'Harga';
	$lang['title_additional_feature']          = 'Fitur tambahan';
	$lang['title_booking']                     = 'Pemesanan';
	$lang['title_calendar']                    = 'Kalender';
	$lang['title_schedule']                    = 'Jadwal';
	$lang['title_chat']                        = 'Percakapan';
	$lang['title_history_chat']                = 'Riwayat Percakapan';
	$lang['title_discount']                    = 'Diskon';
	$lang['title_dashboard']                   = 'Dashboard';
	$lang['title_new_booking']                 = 'Pemesanan baru';
	$lang['title_form_new_booking']            = 'Formulir pemesanan baru';
	$lang['title_detail_transaction']          = 'Detail transaksi';
	$lang['title_office_close']                = 'Toko tutup';
	$lang['title_outside_transaction']         = 'Transaksi diluar';
	$lang['title_user']                        = 'Pengguna';
	$lang['title_profile']                     = 'Profil';
	$lang['title_bank_account']                = 'Akun bank';
	$lang['title_document']                    = 'Dokumen';
	$lang['title_change_password']             = 'Ganti password';
	$lang['title_rating']                      = 'Penilaian';
	$lang['title_service_area']                = 'Area layanan';
	$lang['title_setting_asset_category']      = 'Atur kategori aset';
	$lang['title_transaction_history']         = 'Riwayat transaksi';
	$lang['title_detail_transaction_history']  = 'Detail riwayat transaksi';
	$lang['title_weekend_configuration']       = 'Konfigurasi akhir pekan';
	$lang['title_withdrawal']                  = 'Tarik dana';

	//fields
	$lang['password']      = 'Password';
	$lang['rental_name']   = 'Nama rental';
	$lang['first_name']    = 'Nama depan';
	$lang['last_name']     = 'Nama belakang';
	$lang['repassword']    = 'Konfirmasi password';
	$lang['oldpassword']   = 'Password lama ';
	$lang['newpassword']   = 'Password baru';
	$lang['newrepassword'] = 'Konfirmasi password baru';
	$lang['phone']         = 'Nomor telepon';
	$lang['email']         = 'Email';
	$lang['name']          = 'Nama';

	//labels
	$lang['label_remember_me']                                 = 'Ingat Saya';
	$lang['label_forgot_password']                             = 'Lupa Password';
	$lang['label_no_account'] 	                               = 'Tidak punya akun?';
	$lang['label_signup'] 	   	                               = 'Daftar';
	$lang['label_rcm'] 	   	                                   = 'Rentist Channel Manager';
	$lang['label_login_with_facebook']                         = 'Masuk dengan Facebook';
	$lang['label_login_with_google']                           = 'Masuk dengan Google';
	$lang['label_recover_password']                            = 'Ganti password';
	$lang['label_enter_your_email']                            = 'Masukan email Anda dan instruksi akan dikirimkan kepada Anda!';
	$lang['label_select_type']                                 = 'Pilih tipe';
	$lang['label_personal_details']                            = 'Detail personal';
	$lang['label_phone_number']                                = 'Nomor telepon';
	$lang['label_choose_your_account_type']                    = 'Pilih tipe akun';
	$lang['label_activated_code'] 			                   = 'Kode aktivasi';
	$lang['label_personal'] 	    		                   = 'Personal';
	$lang['label_coorporate']   			                   = 'Perusahaan';
	$lang['label_your_account_needs_some_attention']           = 'Akun Anda butuh tanggapan!';
	$lang['label_phone_verification']                          = 'Verifikasi nomor telepon';
	$lang['label_enter_verification_code_via_sms']             = 'Masukan kode verifikasi dari SMS';
	$lang['label_email_verification']                          = 'Verifikasi email';
	$lang['label_we_are_sending_an_email_for_verification']    = 'Kami mengirimkan email untuk verifikasi';
	$lang['label_personal_data']                               = 'Data pribadi';
	$lang['label_please_complete_your_rental_data']            = 'Mohon lengkapi data rental Anda';
	$lang['label_change_profile_picture']                      = 'Ganti gambar profil';
	$lang['label_information']                                 = 'Informasi';
	$lang['label_password_must_not_be_less_than_8_characters'] = 'Password harus lebih dari 8 karakter';
	$lang['label_password_must_include_at_least_one_number']   = 'Password harus memasukan setidaknya satu angka';
	$lang['label_password_must_include_at_least_one_letter']   = 'assword harus memasukan setidaknya satu huruf';
	$lang['label_asset_category']                              = 'Kategori aset';
	$lang['label_select']                                      = 'Pilih';
	$lang['label_asset_sub_category']                          = 'Sub kategori aset';
	$lang['label_asset_name']                                  = 'Kode aset *Asset ID Name';
	$lang['label_feature_name']                                = 'Judul aset *Headline';
	$lang['label_price']                                       = 'Harga';
	$lang['label_stock']                                       = 'Stok';
	$lang['label_description']                                 = 'Deskripsi';
	$lang['label_weekday_configuration']                       = 'Konfigurasi hari kerja';
	$lang['label_transaction']                                 = 'Transaksi';
	$lang['label_schedule']                                    = 'Jadwal';
	$lang['label_please_wait']                                 = 'Mohon menunggu';
	$lang['label_your_listing']                                = 'Daftar aset';
	$lang['label_title']                                       = 'Judul';
	$lang['label_date']                                        = 'Tanggal';
	$lang['label_full_name']                                   = 'Nama lengkap';
	$lang['label_phone']                                       = 'Nomor telepon';
	$lang['label_email']                                       = 'Email';
	$lang['label_start_date']                                  = 'Tanggal mulai';
	$lang['label_end_date']                                    = 'Tanggal berakhir';
	$lang['label_notes']                                       = 'Catatan';
	$lang['label_nominal_transaction']                         = 'Jumlah transaksi';
	$lang['label_discount_name']                               = 'Nama diskon';
	$lang['label_percentage']                                  = 'Persentase';
	$lang['label_max_amount']                                  = 'Kuota maksimum';
	$lang['label_quantity']                                    = 'Jumlah';
	$lang['label_valid_date']                                  = 'Masa berlaku';
	$lang['label_transaction_code']                            = 'Kode transaksi';
	$lang['label_headline']                                    = 'Judul';
	$lang['label_member_name']                                 = 'Nama member';
	$lang['label_status']                                      = 'Status';
	$lang['label_reason']                                      = 'Alasan';
	$lang['label_name']                                        = 'Nama';
	$lang['label_role']                                        = 'Peran';
	$lang['label_picture']                                     = 'Gambar';
	$lang['label_item_policy']                                 = 'Kebijakan aset';
	$lang['label_country']                                     = 'Negara';
	$lang['label_province']                                    = 'Provinsi';
	$lang['label_city']                                        = 'Kota';
	$lang['label_distric']                                     = 'Kecamatan';
	$lang['label_village']                                     = 'Kelurahan';
	$lang['label_address']                                     = 'Alamat';
	$lang['label_postal_code']                                 = 'Kode Pos';
	$lang['label_street']                                      = 'Jalan';
	$lang['label_bank']                                        = 'Bank';
	$lang['label_account_number']                              = 'Nomor akun';
	$lang['label_account_name']                                = 'Nama akun';
	$lang['label_id_rental']                                   = 'ID Rental';
	$lang['lable_activation_phone']                            = 'Klik jika ingin memverifikasi nomor telepon';
	$lang['lable_activation_email_1']                          = 'Klik jika ingin memverifikasi email';
	$lang['lable_activation_email_2']                          = 'Mohon cek email Anda untuk mengirim ulang';
	$lang['label_here']                                        = 'disini';
	$lang['label_document_category']                           = 'Kategori dokumen';
	$lang['label_file']                                        = 'File';
	$lang['label_action']                                      = 'Aksi';
	$lang['label_rental_name']                                 = 'Nama rental';
	$lang['label_first_name']                                  = 'Nama depan';
	$lang['label_last_name']                                   = 'Nama belakang';
	$lang['label_transaction_completed_on']                    = 'Transaksi selesai pada';
	$lang['label_see_detail_transaction']                      = 'Lihat detail transaksi';
	$lang['label_rating']                                      = 'Rating';
	$lang['label_testimony']                                   = 'Testimoni';
	$lang['label_type']                                        = 'Tipe';
	$lang['label_radius_km']                                   = 'Radius (KM)';
	$lang['label_max_distance_free']                           = 'Jarak maksimum gratis';
	$lang['label_price_per_km']                                = 'Harga per KM';
	$lang['label_total_nominal']                               = 'Total harga';
	$lang['label_delivery_method']                             = 'Metode pengiriman';
	$lang['label_include_insurance']                           = 'Termasuk asuransi';
	$lang['label_payment']                                     = 'Pembayaran';
	$lang['label_payment_gateway']                             = 'Pilihan pembayaran ';
	$lang['label_payment_method']                              = 'Metode pembayaran';
	$lang['label_payment_channel']                             = 'Channel pembayaran';
	$lang['label_payment_status']                              = 'Status pembayaran';
	$lang['label_payment_date']                                = 'Tanggal pembayaran';
	$lang['label_canceled_date']                               = 'Tanggal pembatalan';
	$lang['label_id_member']                                   = 'ID member';
	$lang['label_gender']                                      = 'Jenis kelamin';
	$lang['label_duration']                                    = 'Durasi';
	$lang['label_with_driver']                                 = 'Dengan sopir';
	$lang['label_accepted_date']                               = 'Tanggal diterima';
	$lang['label_ongoing_date']                                = 'Tanggal berlangsung';
	$lang['label_completed_date']                              = 'Tanggal selesai';
	$lang['label_canceled_date']                               = 'Tanggal pembatalan';
	$lang['label_reason']                                      = 'Alasan';
	$lang['label_kredit']                                      = 'Kredit';
	$lang['label_request_kredit']                              = 'Permintaan kredit';
	$lang['label_process_request']                             = 'Proses permintaan';
	$lang['label_days']                                        = 'Hari';
	$lang['label_price_category']                              = 'Kategori harga';
	$lang['label_price_name']                                  = 'Judul harga';
	$lang['label_min_rental_day']                              = 'Minimum sewa';
	$lang['label_destination']                                 = 'Tujuan';
	$lang['label_destination_description']                     = 'Deskripsi tujuan';
	$lang['label_price_received']                              = 'Harga yang akan diterima';
	$lang['label_asset']                                       = 'Aset';
	$lang['label_home']                                        = 'Rumah';
	$lang['label_list_asset']                                  = 'Daftar aset';
	$lang['label_verified']                                    = 'Terverifikasi';
	$lang['label_change_to_inactive']                          = 'Ubah menjadi tidak aktif';
	$lang['label_change_to_active']                            = 'Ubah menjadi aktif';
	$lang['label_asset_detail']                                = 'Detail aset';
	$lang['label_short_summary']                               = 'Ringkasan singkat';
	$lang['label_short_summary_info']                          = 'Tulis deskripsi singkat tentang aset Anda. Ex: Kendaraan ini cocok untuk liburan Anda';
	$lang['label_detail_description']                          = 'Deskripsi lengkap';
	$lang['label_booking_rules']                               = 'Kebijakan menyewa';
	$lang['label_discount_longer_rent']                        = 'Diskon lama sewa';
	$lang['label_insurance']                                   = 'Asuransi';
	$lang['label_reference_insurance_issuer']                  = 'Referensi penerbit asuransi';
	$lang['label_insurance_issuer']                            = 'Penerbit asuransi';
	$lang['label_reference_insurance_policy']                  = 'Kebijakan referensi asuransi';
	$lang['label_insurance_policy']                            = 'Kebijakan asuransi';
	$lang['label_photo']                                       = 'Foto';
	$lang['label_category_photo']                              = 'Kategori foto';
	$lang['label_birth_date']                                  = 'Tanggal lahir';
	$lang['label_photo_info']                                  = 'Apabila anda tidak memiliki asuransi, mohon upload foto STNK asli di bagian photo STNK (hanya untuk keperluan administrasi asuransi).';
	$lang['label_monday']                                      = 'Senin';
	$lang['label_tuesday']                                     = 'Selasa';
	$lang['label_wednesday']                                   = 'Rabu';
	$lang['label_thursday']                                    = 'Kamis';
	$lang['label_friday']                                      = 'Jumat';
	$lang['label_saturday']                                    = 'Sabtu';
	$lang['label_sunday']                                      = 'Minggu';

	$lang['label_pending']                                     = 'Tertunda';
	$lang['label_new_booking']                                 = 'Pesanan baru';
	$lang['label_accepted']                                    = 'Diterima';
	$lang['label_ongoing']                                     = 'Berlangsung';
	$lang['label_canceled']                                    = 'Ditolak';
	$lang['label_completed']                                   = 'Selesai';
	$lang['label_total']                                       = 'Total';
	$lang['label_total_asset']                                 = 'Total aset';
	$lang['label_category']                                    = 'Kategori';
	$lang['label_starting_day']                                = 'Minimum Reservasi Pemesanan';


	//alerts
	$lang['alert_activation_email_success'] = 'Activation email succeeded.';
	$lang['alert_activation_email_failed']  = 'Activation email failed.';
	$lang['alert_delete']                   = 'Are you sure you want to delete this?';
	$lang['alert_delete_choose_one']        = 'Please Select atleast one checkbox.';
	$lang['alert_delete_success']           = 'Data successfully deleted.';
	$lang['alert_delete_failed']            = 'Delete Failed, please try again later.';
	$lang['alert_change_status_to_ongoing'] = 'Are you sure you want to change this transaction status to OnGoin?';
	$lang['alert_delete_document']          = 'Are you sure you want to delete this document?';
	$lang['alert_delete_document_success']  = 'Document successfully deleted.';
	$lang['alert_delete_document_failed']   = 'Document failed to delete.';
	$lang['alert_save_rating']              = 'Save rating success.';
	$lang['alert_no_data']                  = 'No data available.';
	$lang['alert_delete_photo']             = 'Are you sure you want to delete this photo?';
	$lang['alert_delete_photo_success']     = 'Photo successfully deleted.';
	$lang['alert_delete_photo_failed']      = 'Photo failed to delete.';

	//buttons
	$lang['button_login']      = 'Masuk';
	$lang['button_reset']      = 'Atur Ulang';
	$lang['button_next']       = 'Selanjutnya';
	$lang['button_previous']   = 'Sebelumnya';
	$lang['button_finish']     = 'Selesai';
	$lang['button_back']       = 'Kembali';
	$lang['button_save']       = 'Simpan';
	$lang['button_activated']  = 'Actif';
	$lang['button_skip']       = 'Lewati';
	$lang['button_delete']     = 'Hapus';
	$lang['button_add']        = 'Tambah';
	$lang['button_create_new'] = 'Buat Baru';
	$lang['button_find']       = 'Cari';

	//menus
	$lang['menu_dashboard']              = 'Dashboard';
	$lang['menu_my_profile']             = 'Profil';
	$lang['menu_my_profile_rental']      = 'Profil Rental';
	$lang['menu_change_password']        = 'Ganti password';
	$lang['menu_logout']                 = 'Keluar';
	$lang['menu_language']               = 'Bahasa';
	$lang['menu_language_indonesia']     = 'Indonesia ';
	$lang['menu_language_english']       = 'Inggris';
	$lang['menu_language_china']         = 'Cina';
	$lang['menu_language_saudi_arabia']  = 'Arab Saudi';
	$lang['menu_asset']                  = 'Aset';
	$lang['menu_setting_asset_category'] = 'Pengaturan kategori aset';
	$lang['menu_manage_asset']           = 'Pengaturan aset';
	$lang['menu_discount']               = 'Diskon';
	$lang['menu_service_area']           = 'Area layanan';
	$lang['menu_additional_feature']     = 'Tambahan fitur';
	$lang['menu_user']                   = 'Pengguna';
	$lang['menu_transaction_history']    = 'Riwayat transaksi';
	$lang['menu_rating']                 = 'Penilaian';
	$lang['menu_withdrawal']             = 'Tarik dana';
	$lang['menu_chat']                   = 'Percakapan';
	$lang['menu_history_chat']           = 'Riwayat percakapan';
	$lang['menu_office_close']           = 'Toko tutup';
	$lang['menu_outside_transaction']    = 'Transaksi diluar';
	$lang['menu_listing_asset']          = 'Daftar aset';
	$lang['menu_calendar']               = 'Kalender';
	$lang['menu_booking']                = 'Daftar penyewa';
	$lang['menu_help']                   = 'Bantuan';

	//tabs
	$lang['tab_required_detail']            = 'Detail yang diperlukan';
	$lang['tab_change_password']            = 'Ganti password';
	$lang['tab_list_additional_feature']    = 'Daftar fitur tambahan';
	$lang['tab_add_additional_feature']     = 'Tambah fitur tambahan';
	$lang['tab_view_additional_feature']    = 'Lihat fitur tambahan';
	$lang['tab_list_discount']              = 'Daftar diskon';
	$lang['tab_add_discount']               = 'Tambah diskon';
	$lang['tab_view_discount']              = 'Lihat diskon';
	$lang['tab_list_new_booking']           = 'Daftar penyewa baru';
	$lang['tab_detail_transaction_history'] = 'Detail riwayat transaksi';
	$lang['tab_list_office_close']          = 'Daftar toko tutup';
	$lang['tab_add_office_close']           = 'Tambah toko tutup';
	$lang['tab_view_office_close']          = 'Lihat toko tutup';
	$lang['tab_list_outside_transaction']   = 'Daftar transaksi diluar';
	$lang['tab_add_outside_transaction']    = 'Tambah transaksi diluar';
	$lang['tab_view_outside_transaction']   = 'Lihat transaksi diluar';
	$lang['tab_list_user']                  = 'Daftar pengguna';
	$lang['tab_add_user']                   = 'Tambah pengguna';
	$lang['tab_view_user']                  = 'Lihat pengguna';
	$lang['tab_list_policy']                = 'Daftar kebijakan';
	$lang['tab_add_policy']                 = 'Tambah kebijakan';
	$lang['tab_view_policy']                = 'Lihat kebijakan';
	$lang['tab_required_detail']            = 'Detail yang dibutuhkan';
	$lang['tab_address']                    = 'Alamat';
	$lang['tab_bank_account']               = 'Akun bank';
	$lang['tab_document']                   = 'Dokumen';
	$lang['tab_change_password']            = 'Ganti password';
	$lang['tab_list_service_area']          = 'Daftar area layanan';
	$lang['tab_add_service_area']           = 'Tambah area layanan';
	$lang['tab_view_service_area']          = 'Lihat area pelayanan';
	$lang['tab_list_transaction_history']   = 'Daftar riwayat transaksi';
	$lang['tab_detail_transaction_history'] = 'Detail riwayat transaksi';

	$lang['tab_asset']                      = 'Aset';
	$lang['tab_address']                    = 'Alamat';
	$lang['tab_asset_detail']               = 'Detail aset';
	$lang['tab_insurance']                  = 'Asuransi';
	$lang['tab_photo']                      = 'Foto';
	$lang['tab_price']                      = 'Harga';
	$lang['tab_longer_rent']                = 'Lama sewa';
	$lang['tab_booking_rules']              = 'Kebijakan menyewa';

	//valid
	$lang['validate_email_true']  = 'Email dapat digunakan';
	$lang['validate_email_false'] = 'Email salah';
	$lang['validate_phone_less']  = 'Password setidaknya 8 karakter';
	$lang['validate_phone_false'] = 'Password tidak sesuai';
	$lang['validate_phone_true']  = 'Password sesuai';
	$lang['validate_day']         = 'Pengaturan hari tidak boleh kurang dari 3 hari';

	//placeholder
	$lang['placeholder_search_by_address'] = 'Cari berdasarkan alamat';