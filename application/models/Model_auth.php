<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_auth extends CI_Model {

	public function login($username, $password)
	{
		$this->db->select('user.*, kecamatan.nama as nama_kecamatan, desa.nama as nama_desa');
		$this->db->where('username', $username);
		$this->db->where('password', MD5($password));
		$this->db->join('kecamatan', 'kecamatan.id = user.kec_id', 'left');
		$this->db->join('desa', 'desa.id = user.desa_id', 'left');
		$query = $this->db->get('user')->row_array();

		return $query;
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */