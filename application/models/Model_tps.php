<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tps extends CI_Model {

	public function get_all()
	{	
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		} elseif ( $this->session->userdata('role') == 3 ) {
			$this->db->where('tps.desa_id', $this->session->userdata('desa_id'));
		}

		$this->db->select('tps.*, desa.nama as nama_desa'); 
		$this->db->join('desa', 'desa.id = tps.desa_id');
		$this->db->order_by('tps.id', 'desc');
		$query = $this->db->get('tps')->result_array();

		return $query;
	}	
	
	public function get_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tps')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('tps');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */