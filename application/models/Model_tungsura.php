<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tungsura extends CI_Model {

	public function get_all()
	{
		if ($this->session->userdata('role') == 2) {
			$this->db->where('kecamatan.id', $this->session->userdata('kec_id'));
		} elseif ($this->session->userdata('role') == 3) {
			$this->db->where('desa.id', $this->session->userdata('desa_id'));
		}
		
		$this->db->select('tungsura.*,
							cakades.nama as nama_cakades,
							cakades.no_urut,
							tps.nama as nama_tps,
							tps.alamat,
							tps.hak_pilih,
							tps.tungsura_tidak_sah,
							desa.nama as nama_desa,
							kecamatan.nama as nama_kecamatan,
							kecamatan.zona_kec,
							tungsura.jumlah_suara,
							tungsura.created_at,
							`user`.`nama` as nama_user'); 

		$this->db->join('cakades', 'cakades.id = tungsura.cakades_id');
		$this->db->join('tps', 'tps.id = tungsura.tps_id');
		$this->db->join('desa', 'desa.id = cakades.desa_id');
		$this->db->join('kecamatan', 'kecamatan.id = desa.kec_id');
		$this->db->join('`user`', '`user`.`id` = tungsura.created_by');
		$this->db->order_by('tungsura.id', 'desc');
		$query = $this->db->get('tungsura')->result_array();

		return $query;
	}	

	public function get_all_desa_by_user()
	{
		if ($this->session->userdata('role') == 2) {
			$this->db->where('kecamatan.id', $this->session->userdata('kec_id'));
		} elseif ($this->session->userdata('role') == 3) {
			$this->db->where('desa.id', $this->session->userdata('desa_id'));
		}

		$this->db->select('desa.*, kecamatan.nama as nama_kecamatan, kecamatan.zona_kec'); 

		$this->db->join('kecamatan', 'kecamatan.id = desa.kec_id');
		$this->db->order_by('desa.id', 'desc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}

	public function get_all_cakades_by_desa($id)
	{
		
		$this->db->where('cakades.desa_id', $id);

		$this->db->select('*'); 

		$this->db->order_by('cakades.nama', 'asc');
		$query = $this->db->get('cakades')->result_array();

		return $query;
	}

	public function get_all_tps_by_desa($id)
	{
		$this->db->select('tps_id'); 
		$this->db->group_by("tps_id");
		$queryTungsura = $this->db->get('tungsura')->result_array();
		$tps = array();
		foreach ($queryTungsura as $val) {
			$tps[] = $val['tps_id'];
		}

		$this->db->where('tps.desa_id', $id);
		if ( count($tps) > 0 ) {
			$this->db->where_not_in('tps.id', $tps);
		}
		$this->db->select('*'); 
		$this->db->order_by('tps.nama', 'asc');
		$query = $this->db->get('tps')->result_array();

		return $query;
	}

	public function get_all_tps_by_desa_on_view($id)
	{
		$this->db->where('tps.desa_id', $id);
		$this->db->select('*'); 
		$this->db->order_by('tps.nama', 'asc');
		$query = $this->db->get('tps')->result_array();

		return $query;
	}

	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('nama', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('tungsura.id', $id);
		$this->db->select('tungsura.*, cakades.desa_id'); 

		$this->db->join('cakades', 'cakades.id = tungsura.cakades_id');
		$query = $this->db->get('tungsura')->row_array();

		return $query;
	}	
	
	public function get_detail_by_tps_cakades($cakades_id, $tps_id)
	{
		$this->db->where('cakades_id', $cakades_id);
		$this->db->where('tps_id', $tps_id);
		$query = $this->db->get('tungsura')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('tungsura');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */