<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_home extends CI_Model {

	public function get_all( $post = array() )
	{
		$resp = array();
		if ( @$post['kec_id'] != "" ) {
			$this->db->where('id', $post['kec_id']);
		}
		$this->db->order_by('kecamatan.nama', 'asc');
		$query = $this->db->get('kecamatan')->result_array();

		$resp['data'] = $query;
		foreach ($resp['data'] as $index => $data) {

			if ( @$post['desa_id'] != "" ) {
				$this->db->where('id', $post['desa_id']);
			}
			$this->db->where('kec_id', $data['id']);
			$this->db->order_by('desa.nama', 'asc');
			$queryDesa = $this->db->get('desa')->result_array();

			$resp['data'][$index]['data_desa'] = $queryDesa;
			foreach ($resp['data'][$index]['data_desa'] as $indexDesa => $dataDesa) {
				$this->db->where('desa_id', $dataDesa['id']);
				$this->db->order_by('tps.nama', 'asc');
				$queryTps = $this->db->get('tps')->result_array();

				$this->db->where('desa_id', $dataDesa['id']);
				$this->db->order_by('cakades.no_urut', 'asc');
				$queryCakades = $this->db->get('cakades')->result_array();

				$resp['data'][$index]['data_desa'][$indexDesa]['data_tps']     = $queryTps;
				$resp['data'][$index]['data_desa'][$indexDesa]['data_cakades'] = $queryCakades;
				foreach ($resp['data'][$index]['data_desa'][$indexDesa]['data_cakades'] as $indexCakades => $dataCakades) {
					$resp['data'][$index]['data_desa'][$indexDesa]['data_cakades'][$indexCakades]['data_tps'] = $queryTps;

					foreach ($resp['data'][$index]['data_desa'][$indexDesa]['data_cakades'][$indexCakades]['data_tps'] as $indexTps => $dataTps) {
						$this->db->where('cakades_id', $dataCakades['id']);
						$this->db->where('tps_id', $dataTps['id']);
						$queryCakades = $this->db->get('tungsura')->row_array();

						$resp['data'][$index]['data_desa'][$indexDesa]['data_cakades'][$indexCakades]['data_tps'][$indexTps]['jumlah_suara'] = $queryCakades['jumlah_suara'];
					}
				}
			}
		}

		// pre($resp);

		return $resp;
	}

	public function get_chart_by_desa($desa_id)
	{
		$this->db->where('desa_id', $desa_id);
		$this->db->order_by('no_urut', 'asc');
		$query = $this->db->get('cakades')->result_array();

		$resp = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['datasets'][$index]['label'] 			 = $data['nama'];
			$resp['datasets'][$index]['backgroundColor'] = $color;
			$resp['datasets'][$index]['borderColor']     = $color;

			$this->db->select('sum(jumlah_suara) as jumlah_suara');
			$this->db->where('desa_id', $desa_id);
			$this->db->where('cakades_id', $data['id']);
			$this->db->join('cakades', 'cakades.id = tungsura.cakades_id');
			$queryTungsura = $this->db->get('tungsura')->row_array();
			$resp['datasets'][$index]['data'][]          = (int)$queryTungsura['jumlah_suara'];
		}
		// pre($resp);

		return $resp;
	}
	
	public function get_chart_by_tps($desa_id)
	{
		$resp = array();

		$this->db->where('desa_id', $desa_id);
		$this->db->order_by('nama', 'asc');
		$queryTps = $this->db->get('tps')->result_array();
		foreach ($queryTps as $index => $data) {
			$resp['labels'][] = $data['nama'];
		}

		$this->db->where('desa_id', $desa_id);
		$this->db->order_by('no_urut', 'asc');
		$query = $this->db->get('cakades')->result_array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['datasets'][$index]['label'] 			 = $data['nama'];
			$resp['datasets'][$index]['backgroundColor'] = $color;
			$resp['datasets'][$index]['borderColor']     = $color;
			foreach ($queryTps as $indexTps => $dataTps) {
				$this->db->select('jumlah_suara');
				$this->db->where('desa_id', $desa_id);
				$this->db->where('cakades_id', $data['id']);
				$this->db->where('tps_id', $dataTps['id']);
				$this->db->join('cakades', 'cakades.id = tungsura.cakades_id');
				$queryTungsura = $this->db->get('tungsura')->row_array();
				$resp['datasets'][$index]['data'][]          = (int)$queryTungsura['jumlah_suara'];

			}
		}

		return $resp;
	}
	
	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('nama', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}
	
	public function get_all_kecamatan($post)
	{
		$resp = array();

		if ($this->session->userdata('role') != 1 ) {
			if ( @$post['kec_id'] != "" ) {
				$this->db->where('id', $post['kec_id']);
			}
		}
	
		$this->db->order_by('nama', 'asc');
		$query = $this->db->get('kecamatan')->result_array();
		// pre(json_encode($resp));

		return $query;
    }
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */