<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

	public function get_all()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('user.kec_id', $this->session->userdata('kec_id'));
		} elseif ( $this->session->userdata('role') == 3 ) {
			$this->db->where('user.desa_id', $this->session->userdata('desa_id'));
		}
		
		$this->db->select('`user`.*, desa.nama as nama_desa, kecamatan.nama as nama_kecamatan'); 

		$this->db->join('desa', 'desa.id = `user`.`desa_id`', 'left');
		$this->db->join('kecamatan', 'kecamatan.id = `user`.`kec_id`', 'left');
		$this->db->order_by('user.id', 'desc');
		$query = $this->db->get('user')->result_array();

		return $query;
	}	
	
	public function get_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('user')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('user');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */