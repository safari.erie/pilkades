<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<style>
    .hscroll {
        width: 100%;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>

<!-- $content -->
<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <center>
            <h4 class="text-themecolor"><strong>SELAMAT DATANG DI APLIKASI PILKADES SERENTAK 2020</strong></h4>
        </center>
    </div>
    
</div>

<div class="card">
    <div class="card-body collapse show">
        <div class="card">
            <div class="card-header">
                <strong>Filter Data:</strong>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Kecamatan
                                </label>
                                <select id="kec_id" name="kec_id" class="select2 form-control" <?php echo @$post['kec_id_disabled']; ?>>
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_kecamatan as $data) {
                                            if ($post['kec_id'] == $data['id']) {
                                                $selected1 = "selected";
                                            } else {
                                                $selected1 = "";
                                            }
                                    ?>
                                        <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                            <?php echo $data['nama']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa
                                </label>
                                <select id="desa_id" name="desa_id" class="select2 form-control" <?php echo @$post['desa_id_disabled']; ?>>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: auto;">
                            <div class="form-actions pull-left" style="margin-right:10px;">
                                <button type="submit" id="filter" name="filter" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Filter
                                </button>
                            </div>

                            <div class="form-actions pull-left" style="margin-right:10px;">
                                <button type="submit" id="reset" name="reset" class="btn btn-danger"> 
                                    <i class="fa fa-times"></i> Reset Filter
                                </button>
                            </div>

                            <div class="form-actions pull-left">
                                <button type="submit" id="export" name="export" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Export Data
                                </button>
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
    foreach ($resp['data'] as $data) {
?>
    <div class="card">
        <div class="card-body collapse show">
            
            <div class="card">
                <h3>Kecamatan: <?php echo $data['nama']; ?></h3>

                <?php
                    foreach ($data['data_desa'] as $dataDesa) {
                ?>
                    <div class="card-header">
                        > Desa: <?php echo $dataDesa['nama']; ?>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="hscroll">
                                    <table class="table table-striped table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th rowspan="5" style="font-weight:bold;">No. Urut</th>
                                                <th rowspan="5" style="font-weight:bold;">Calon Kepala Desa</th>
                                                <th colspan="<?php echo count($dataDesa['data_tps'])?>" style="text-align:center; font-weight:bold;">Jumlah Suara</th>
                                                <th rowspan="5" style="text-align:center; font-weight:bold;">Total Suara</th>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <?php
                                                    $totalHakPilih = 0;
                                                    foreach ($dataDesa['data_tps'] as $dataTps) {
                                                        $totalHakPilih += $dataTps['hak_pilih']
                                                ?>
                                                    <th style="font-weight:bold;"><?php echo $dataTps['nama']; ?></th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                            <tr>
                                                <?php
                                                    foreach ($dataDesa['data_tps'] as $dataTps) {
                                                ?>
                                                    <th style="text-align:center; font-weight:bold;">Hak Pilih: <i><?php echo $dataTps['hak_pilih']; ?></i></th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                            <tr>
                                                <?php
                                                    foreach ($dataDesa['data_tps'] as $dataTps) {
                                                ?>
                                                    <th style="text-align:center; font-weight:bold;">Suara Tidak Sah: <i style="color:red"><?php echo $dataTps['tungsura_tidak_sah']; ?></i></th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                            <tr>
                                                <?php
                                                    foreach ($dataDesa['data_tps'] as $dataTps) {
                                                ?>
                                                    <th style="text-align:center; font-weight:bold; color:green; "><i>Suara Sah</i></th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $totalKeseluruhan = 0;
                                                $color = "";
                                                $alert = "";
                                                foreach ($dataDesa['data_cakades'] as $dataCakades) {
                                            ?>
                                                <tr>
                                                    <td><?php echo $dataCakades['no_urut']; ?></td>
                                                    <td><?php echo $dataCakades['nama']; ?></td>
                                                    
                                                    <?php
                                                        $total = 0;
                                                        foreach ($dataCakades['data_tps'] as $dataTps) {
                                                            $total += $dataTps['jumlah_suara'];
                                                    ?>
                                                        <td style="text-align:center; color:green;"><i><?php echo $dataTps['jumlah_suara']; ?></i></td>
                                                    <?php
                                                        }
                                                    ?>
                                                    <td style="text-align:center;"><?php echo $total; ?></td>
                                                </tr>
                                            <?php
                                                    $totalKeseluruhan += $total; 
                                                    if ( $totalKeseluruhan > $totalHakPilih) {
                                                        $color = "color:red;";
                                                        $alert = "Total keseluruhan suara tidak boleh lebih dari total hak suara. Silahakan perbaiki kembali perhitungan suara.";
                                                    }
                                                } 
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="<?php echo ( count($dataDesa['data_tps']) + 2 )?>" style="text-align:right; font-weight:bold;">
                                                    Total Keseluruhan Suara : 
                                                </td>
                                                <td style="text-align:center; font-weight:bold; <?php echo $color; ?>">
                                                    <?php echo $totalKeseluruhan; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="<?php echo ( count($dataDesa['data_tps']) + 2 )?>" style="text-align:right; font-weight:bold;">
                                                    Total Hak Pilih : 
                                                </td>
                                                <td style="text-align:center; font-weight:bold;">
                                                    <?php echo $totalHakPilih; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="<?php echo ( count($dataDesa['data_tps']) + 3 )?>" style="text-align:center; font-weight:bold; color:red;">
                                                    <?php echo $alert; ?>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body fit">
                                        <table class="table">
                                            <tr>
                                                <td width="50%">
                                                <div class="row">
                                                        <div class="col-md-12" height="400px">
                                                            <h3>Grafik Desa</h3>
                                                            <canvas id="chartDesa<?php echo $dataDesa['id']; ?>" height="100%"></canvas>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="50%">
                                                    <div class="row">
                                                        <div class="col-md-12" height="400px">
                                                            <h3>Grafik Desa By TPS</h3>
                                                            <canvas id="chartTps<?php echo $dataDesa['id']; ?>" height="100%"></canvas>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $.ajax({
                                    dataType: "json",
                                    url: '<?php echo base_url("rcmadmin/home/get_chart_by_desa"); ?>/<?php echo $dataDesa['id']; ?>',
                                    success: function (resp) {

                                        var ctx = document.getElementById('chartDesa<?php echo $dataDesa['id']; ?>').getContext('2d');
                                        var chart = new Chart(ctx, {
                                            type: 'bar',
                                            data: resp,
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });

                                    }
                                });

                                $.ajax({
                                    dataType: "json",
                                    url: '<?php echo base_url("rcmadmin/home/get_chart_by_tps"); ?>/<?php echo $dataDesa['id']; ?>',
                                    success: function (resp) {

                                        var ctx = document.getElementById('chartTps<?php echo $dataDesa['id']; ?>').getContext('2d');
                                        var chart = new Chart(ctx, {
                                            type: 'bar',
                                            data: resp,
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });

                                    }
                                });
                            </script>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
<?php
    }
?>

<script>
    <?php
        if ( @$post['kec_id'] != "" && @$post['desa_id'] != "") {
    ?>
        var kec_id     = '<?php echo $post['kec_id']; ?>';
        var desa_id    = '<?php echo $post['desa_id']; ?>';
        var temp1 = "";
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("rcmadmin/home/get_all_desa_by_kecamatan"); ?>/' + kec_id,
            success: function (data) {
              $("#desa_id").empty(); 
              $("#desa_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
              $(data).each(function(i) {
                if ( desa_id == data[i].id ) {
                  temp1 = "selected";
                } else {
                  temp1 = "";
                }

                $("#desa_id").append("<option " + temp1 + " value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
              });
            }
        });
    <?php
        } elseif ( @$post['kec_id'] != "" && @$post['desa_id'] == "") {
    ?>
        var id = '<?php echo $post['kec_id']; ?>';;
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("rcmadmin/home/get_all_desa_by_kecamatan"); ?>/' + id,
            success: function (data) {
                $("#desa_id").empty(); 
                $("#desa_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                $(data).each(function(i) {
                    $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
                });
            }
        });
    <?php
        }
    ?>

    $('#kec_id').change(function () {
        var id = $(this).val();
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("rcmadmin/home/get_all_desa_by_kecamatan"); ?>/' + id,
            success: function (data) {
                $("#desa_id").empty(); 
                $("#desa_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                $(data).each(function(i) {
                    $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
                });
            }
        });
    });
</script>