<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/user">
                List User
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                View User
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nama User <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="nama" value="<?php echo @$post['nama']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Username <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="username" value="<?php echo @$post['username']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Password <span class="text-danger">*</span>
                                    </label>
                                    <input type="password" class="form-control" name="password" value="<?php echo @$post['password']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Role <span class="text-danger">*</span>
                                    </label>
                                    <select name="role" id="role" class="select2 form-control" onchange="submit()" required>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($role as $index => $val) {
                                                if ($post['role'] == $index) {
                                                    $selected1 = "selected";
                                                } else {
                                                    $selected1 = "";
                                                }
                                        ?>
                                            <option value="<?php echo $index; ?>" <?php echo $selected1; ?>>
                                                <?php echo $val; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <?php if ( @$post['role'] == 2 ) {?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Kecamatan <span class="text-danger">*</span>
                                        </label>
                                        <select name="kec_id" id="kec_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                                    if ($post['kec_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['nama']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if ( @$post['role'] == 3 ) {?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <select name="desa_id" id="desa_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_desa as $data) {
                                                    if ($post['desa_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['nama']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

