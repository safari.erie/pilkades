<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/cakades">
                List Calon Kepala Desa
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                Add Calon Kepala Desa
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-1" style="margin:auto;">
                            <button type="button" class="btn btn-danger" onclick="remove(this)"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa <span class="text-danger">*</span>
                                </label>
                                <select name="desa_id[]" id="desa_id" class="select2 form-control" required>
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_desa as $data) {
                                    ?>
                                        <option value="<?php echo $data['id']; ?>">
                                            <?php echo $data['nama']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Nama <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nama[]" value="" required>
                            </div>
                        </div>

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    No. Urut <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="no_urut[]" value="" required>
                            </div>
                        </div>
                    </div>

                    <div id="form-tambah-inputan">
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="text-align:center;">
                            <a href="javascript:void(0)" id="tambah-inputan"><strong><i class="fa fa-plus"></i> Tambah Inputan</strong></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        $("#tambah-inputan").on( "click", function() {
            var html = "<div class=\"row\">" + 
                "<div class=\"col-md-1\" style=\"margin:auto;\">" +
                    "<button type=\"button\" class=\"btn btn-danger remove\" onclick=\"remove(this)\"><i class=\"fa fa-times\"></i></button>" +
                "</div>"+
                "<div class=\"col-md-3\">" +
                    "<div class=\"form-group\">" +
                        "<label class=\"control-label\">"+
                            "Desa <span class=\"text-danger\">*</span>" +
                        "</label>" + 
                        "<select name=\"desa_id[]\" id=\"desa_id\" class=\"select2 form-control\" required>"+
                            "<option value=\"\"> --- Pilih --- </option>" +
                            <?php
                                foreach ($list_desa as $data) {
                            ?>
                                "<option value=\"<?php echo $data['id']; ?>\">" +
                                    "<?php echo $data['nama']; ?>" + 
                                "</option>" + 
                            <?php
                                }
                            ?>
                        "</select>" +
                    "</div>" +
                "</div>" +

                "<div class=\"col-md-4 form-radius\">" +
                    "<div class=\"form-group\">" +
                        "<label class=\"control-label\">" +
                            "Nama <span class=\"text-danger\">*</span>" +
                        "</label>" +
                        "<input type=\"text\" class=\"form-control\" name=\"nama[]\" value=\"\" required>" +
                    "</div>" +
                "</div>" + 

                "<div class=\"col-md-4 form-radius\">" +
                    "<div class=\"form-group\">" +
                        "<label class=\"control-label\">" +
                            "No. Urut <span class=\"text-danger\">*</span>" +
                        "</label>" +
                        "<input type=\"number\" class=\"form-control\" name=\"no_urut[]\" value=\"\" required>" +
                    "</div>" +
                "</div>" +
            "</div>";

            $("#form-tambah-inputan").append(html);
        });

        // $('.remove').on( "click", function() {
        //     alert('berhasil');
        //     console.log($(this));
        //     // $(this).parent().parent().remove();
        // });
    });

    function remove(here) {
        $(here).parent().parent().remove();
    }
</script>