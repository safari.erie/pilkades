<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/tps">
                List TPS
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                View TPS
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Nama TPS <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nama" value="<?php echo $tps['nama']; ?>" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa <span class="text-danger">*</span>
                                </label>
                                <select name="desa_id" id="desa_id" class="select2 form-control" required>
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_desa as $data) {
                                            if ($tps['desa_id'] == $data['id']) {
                                                $selected1 = "selected";
                                            } else {
                                                $selected1 = "";
                                            }
                                    ?>
                                        <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                            <?php echo $data['nama']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Zonasi Desa <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="zona_desa" value="<?php echo $tps['zona_desa']; ?>" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Alamat TPS <span class="text-danger">*</span>
                                </label>
                                <textarea name="alamat" class="form-control" required><?php echo $tps['alamat']; ?></textarea>
                            </div>
                        </div>

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Hak Pilih <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="hak_pilih" value="<?php echo $tps['hak_pilih']; ?>" required>
                            </div>
                        </div>

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Perhitungan Suara Tidak Sah <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="tungsura_tidak_sah" value="<?php echo $tps['tungsura_tidak_sah']; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

