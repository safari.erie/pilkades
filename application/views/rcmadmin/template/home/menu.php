        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>rcmadmin/home">
                        <b>
                            P 
                        </b>
                        <span style="align:center">
                            | PILKADES 2020
                        </span>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> 
                            <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark" href="javascript:void(0)">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item"> 
                            <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)">
                                <i class="icon-menu"></i>
                            </a> 
                        </li>
                    </ul>

                    <ul class="navbar-nav my-lg-0">

                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="hidden-md-down" style="color:black;">
                                    <?php echo $this->session->userdata('nama'); ?> 
                                    <strong> As </strong> 
                                    <?php echo role($this->session->userdata('role')); ?> 
                                    <?php 
                                        if ( $this->session->userdata('role') == 2 ) {
                                            echo "<i style='font-weight:bold'>" . $this->session->userdata('nama_kecamatan') . "</i>";
                                        } elseif ( $this->session->userdata('role') == 3 ) {
                                            echo "<i style='font-weight:bold'>" . $this->session->userdata('nama_desa') . "</i>";
                                        }
                                    ?>
                                    <i class="icon-options-vertical"></i>
                                </span>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- <a href="<?php //echo base_url(); ?>rcmadmin/auth/change_password" class="dropdown-item"><i class="ti-user"></i> 
                                    Ubah Kata Sandi
                                </a> -->
                                
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo base_url(); ?>rcmadmin/auth/logout" class="dropdown-item"><i class="fa fa-power-off"></i>
                                    Keluar
                                </a>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>rcmadmin/home" aria-expanded="false">
                                <i class="fa fa-home"></i>
                                <span class="hide-menu">Dashbboard</span>
                            </a>
                        </li>

                        <?php
                            if ($this->session->userdata['role'] == 1) {
                        ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                    <i class="ti-archive"></i>
                                    <span class="hide-menu">Master Data</span>
                                </a>
                                <ul aria-expanded="false" class="collapse" style="margin-top: 5px;">
                
                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/kecamatan">
                                            Kecamatan
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/desa">
                                            Desa
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/tps">
                                            TPS
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/cakades">
                                            Calon Kepala Desa
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/user">
                                            User
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>rcmadmin/tungsura" aria-expanded="false">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span class="hide-menu">Perhitungan Suara</span>
                                </a>
                            </li>
                        <?php
                            } elseif ($this->session->userdata['role'] == 2) {
                        ?>

                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                    <i class="ti-archive"></i>
                                    <span class="hide-menu">Master Data</span>
                                </a>
                                <ul aria-expanded="false" class="collapse" style="margin-top: 5px;">

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/tps">
                                            TPS
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/cakades">
                                            Calon Kepala Desa
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/user">
                                            User
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>rcmadmin/tungsura" aria-expanded="false">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span class="hide-menu">Perhitungan Suara</span>
                                </a>
                            </li>
                        <?php
                            } elseif ($this->session->userdata['role'] == 3) {
                        ?>

                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                    <i class="ti-archive"></i>
                                    <span class="hide-menu">Master Data</span>
                                </a>
                                <ul aria-expanded="false" class="collapse" style="margin-top: 5px;">

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/tps">
                                            TPS
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>rcmadmin/cakades">
                                            Calon Kepala Desa
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>rcmadmin/tungsura" aria-expanded="false">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span class="hide-menu">Perhitungan Suara</span>
                                </a>
                            </li>
                        <?php
                            }
                        ?>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        <div class="page-wrapper">
            <div class="container-fluid">
                