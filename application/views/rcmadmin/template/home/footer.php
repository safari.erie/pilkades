            </div>
        </div>
        <footer class="footer">
            © 2020 Pilkades Serentak
            <audio id="beep-notif" preload="auto">
                <source src="<?php echo base_url(); ?>sound/plucky.mp3"></source>
                <source src="<?php echo base_url(); ?>sound/plucky.ogg"></source>
                Your browser isn't invited for super fun audio time.
            </audio>
        </footer>
    </div>


    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/home/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>js/home/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/home/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/home/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/home/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/home/custom.min.js"></script>
    <!--Custom select -->
    <script src="<?php echo base_url(); ?>js/home/select2.min.js"></script>
    <!--Data Tables -->
    <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>js/fancybox/jquery.fancybox.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>js/toast/jquery.toast.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url() ?>js/rentist.js"></script> -->
    <script src="<?php echo base_url() ?>js/global.js"></script>
    <script src="<?php echo base_url() ?>js/phone/intlTelInput.js"></script>
    <!-- Add js on controller -->
    <?php
        if( ! empty( $js ) ) { 
            foreach ($js as $linkjs) echo '<script src="' . base_url() . $linkjs . '.js" /></script>', "\n"; 
        }
    ?>
    
    <script src="<?php echo base_url() ?>js/tool.js"></script>

    <script src="<?php echo base_url() ?>js/datepicker/moment.js"></script>
</body>

</html>