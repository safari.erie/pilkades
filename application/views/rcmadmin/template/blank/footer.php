            </div>
        </div>
    </div>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/blank/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>js/home/select2.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url(); ?>js/rentist.js"></script> -->
    <script src="<?php echo base_url() ?>js/global.js"></script>

    <?php
        if( ! empty( $js ) ) { 
            foreach ($js as $linkjs) echo '<script src="' . base_url() . $linkjs . '.js" /></script>', "\n"; 
        }
    ?>
    
    <!-- <script src="<?php //echo base_url() ?>js/tool.js"></script> -->
    <script type="text/javascript">
        $(function() {
            $('.select2').select2();
            $(".preloader").fadeOut();
        });
        
    </script>
</body>

</html>