<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                List Perhitungan Suara
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/tungsura/add">
                Add Perhitungan Suara
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">
                		<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0)" class="btn btn-danger deleteall">
                                    <i class="fa fa-times"></i> <?php echo $this->lang->line('button_delete'); ?>
                                </a>
							</div>
						</div>
						<div class="table-responsive">						
	                		<input type="hidden" value="<?php echo base64_encode('rcmadmin/tungsura/delete'); ?>">
				            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_choose_one')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_success')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_failed')); ?>">

				            <table id="table-rentist" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>#</th>
					        			<th>Kecamatan</th>
					        			<th>Desa</th>
					        			<th>Calon Kepala Desa</th>
					        			<th>TPS</th>
					        			<th>Hak Pilih</th>
					        			<th>Suara Tidak Sah</th>
					        			<th>Jumlah Suara</th>
					        			<th>Input Oleh</th>
					        			<th>Tanggal Input</th>
					        			<th>#</th>
				                    </tr>
				                </thead>
				                <tbody>
				                	<?php
				                		$no = 0;
				                		foreach ($list_tungsura as $data) {
				                			$no++;
				                	?>
					                    <tr id="id-<?php echo $data['id']; ?>">
				                            <td><input type="checkbox" value="<?php echo $data['id']; ?>"></td>
                                            <td>
                                                <?php echo ! empty($data['nama_kecamatan']) ? $data['nama_kecamatan'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['nama_desa']) ? $data['nama_desa'] : "-"; ?>
                                            </td>
						        			<td>
                                                <?php echo ! empty($data['nama_cakades']) ? $data['nama_cakades'] : "-"; ?>
                                            </td>
						        			<td>
                                                <?php echo ! empty($data['nama_tps']) ? $data['nama_tps'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['hak_pilih']) ? $data['hak_pilih'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['tungsura_tidak_sah']) ? $data['tungsura_tidak_sah'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['jumlah_suara']) ? $data['jumlah_suara'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['nama_user']) ? $data['nama_user'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['created_at']) ? $data['created_at'] : "-"; ?>
                                            </td>
						        			<td>
						        				<a href="<?php echo base_url(); ?>rcmadmin/tungsura/view/<?php echo $data['id']; ?>" class="btn btn-info" style="margin-top: -7px;">
				                                    <i class="fa fa-edit"></i>
				                                </a>
						        			</td>
					                    </tr>
				                    <?php
				                		}
				                	?>
				                </tbody>
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('.deleteall').click(function(){
        var url    = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(0).val());
        var alert1 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(1).val());
        var alert2 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(2).val());
        var alert3 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(3).val());
        var alert4 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(4).val());

        swal({
            // title: alert1,
            text: alert1,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = [];

                $(this).parent().parent().parent().find('tbody :checkbox:checked').each(function(i){
                    id[i] = $(this).val();
                });

                if ( id.length === 0) {
            
                    swal(alert2, {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
            
                } else {
            
                    $.ajax({
                        url: '<?php echo base_url(); ?>' + url,
                        method:'POST',
                        data:{id:id},
                        success:function(data) {
                            if (data == "success") {
                                for ( var i = 0; i < id.length; i++ ) {
                                    $('tr#id-'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#id-'+id[i]+'').fadeOut('slow');
                                }

                                swal(alert3, {
                                    icon: "success",
                                    buttons: false,
                                    timer: 2000,
                                });
                            } else {
                                swal(alert4, {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        }
                    });
                }
            }
        });
    });
</script>