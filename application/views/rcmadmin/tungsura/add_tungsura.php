<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/tungsura">
                List Perhitungan Suara
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                Add Perhitungan Suara
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                if ( $this->session->userdata('role') == 1 ) {
                            ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Kecamatan <span class="text-danger">*</span>
                                        </label>
                                        <select id="kec_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>">
                                                    <?php echo $data['nama']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <select id="desa_id" class="select2 form-control" required>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                } else {
                            ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <select id="desa_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_desa as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>">
                                                    <?php echo $data['nama']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        TPS <span class="text-danger">*</span>
                                    </label>
                                    <select name="tps_id" id="tps_id" class="select2 form-control" required>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-striped table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th colspan="3">
                                            <div class="row">
                                                <div class="col-md-10" style="text-align:right; margin:auto;">
                                                    Perhutangan Suara Tidak Sah :
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="number" name="tungsura_tidak_sah" value="0" class="form-control" required>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Nomor Urut</th>
                                        <th>Calon Kepala Desa</th>
                                        <th>Jumlah Suara</th>
                                    </tr>
                                </thead>
                                <tbody id="form-cakades">
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" align="right" style="font-weight: bold;">
                                            <h3>Total Keseluruhan Suara :</h3>
                                        </td>
                                        <td id="form-total-suara" style="font-weight: bold;">
                                            <h3 id="form-total-suara"></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" style="font-weight: bold;">
                                            <h3>Total Hak Pilih :</h3>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <h3 id="form-hak-pilih"></h3>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>  

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" id="save" name="save" class="btn btn-success" disabled> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {

        $('#form-cakades').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            })
        })
        $('#form-cakades').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll')
        })

        <?php
            if ( $this->session->userdata('role') == 1 ) {
        ?>
                $('#kec_id').change(function () {
                    var id = $(this).val();

                    if (id == "") {
                        $("#form-cakades").empty();
                    }

                    $.ajax({
                        dataType: "json",
                        url: '<?php echo base_url("rcmadmin/tungsura/get_all_desa_by_kecamatan"); ?>/' + id,
                        success: function (data) {
                            $("#desa_id").empty(); 
                            $("#desa_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                            $(data).each(function(i) {
                                $("#desa_id").append("<option value=\"" + data[i].id + "-" + data[i].hak_pilih + "\">" + data[i].nama + "</option>");
                            });
                        }
                    });
                });

                $('#desa_id').change(function () {
                    // var result = $(this).val().split('-');
                    // var id = result[0];
                    // var hak_pilih = result[1];

                    // $('#form-hak-pilih').text(hak_pilih);

                    var id = $(this).val();
                    if (id == "") {
                        $("#form-cakades").empty();
                    }

                    $.ajax({
                        dataType: "json",
                        url: '<?php echo base_url("rcmadmin/tungsura/get_all_tps_by_desa"); ?>/' + id,
                        success: function (data) {
                            $("#tps_id").empty(); 
                            $("#tps_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                            $(data).each(function(i) {
                                $("#tps_id").append("<option value=\"" + data[i].id + "-" + data[i].hak_pilih + "\">" + data[i].nama + "</option>");
                            });
                        }
                    });
                });
        <?php
            } else {
        ?>
                $('#desa_id').change(function () {
                    var id = $(this).val();
                    if (id == "") {
                        $("#form-cakades").empty();
                    }

                    $.ajax({
                        dataType: "json",
                        url: '<?php echo base_url("rcmadmin/tungsura/get_all_tps_by_desa"); ?>/' + id,
                        success: function (data) {
                            $("#tps_id").empty(); 
                            $("#tps_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                            $(data).each(function(i) {
                                $("#tps_id").append("<option value=\"" + data[i].id + "-" + data[i].hak_pilih + "\">" + data[i].nama + "</option>");
                            });
                        }
                    });
                });
        <?php
            }
        ?>

        $('#tps_id').change(function () {
            var id = $('#desa_id').val();

            var result = $(this).val().split('-');
            var tps_id = result[0];
            var hak_pilih = result[1];

            $('#form-hak-pilih').text(hak_pilih);

            if (tps_id == "") {
                $("#form-cakades").empty();
            } else {
                $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url("rcmadmin/tungsura/get_all_cakades_by_desa"); ?>/' + id,
                    success: function (data) {
                        $("#form-cakades").empty(); 
                        $(data).each(function(i) {
                            $("#form-cakades").append("<tr><td>" + data[i].no_urut + "</td><td>" + data[i].nama + "</td><td><input class=\"form-control\" type=\"number\" name=\"jumlah_suara[" + data[i].id + "]\" value=\"0\" onkeyup=\"sum()\" onscroll=\"sum()\" required></td></tr>");
                        });
                    }
                });
            }
        });
    });

    function sum(){
        var total = 0;
        $('#form-cakades input[type=number]').each(function() {
            total += parseInt($(this).val());
        })

        var color = ""
        hak_pilih = parseInt($('#form-hak-pilih').text());
        if (total > hak_pilih) {
            alert("Total keseluruhan suara melebihi total hak pilih.")
            $('#save').attr("disabled", true);
            color = "style='color:red'";
        } else {
            $('#save').removeAttr("disabled");
        }

        $('#form-total-suara').html("<h3 " + color + ">" + total + "</h3>");
    }
</script>