<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>rcmadmin/tungsura">
                List Perhitungan Suara
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                View Perhitungan Suara
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Desa <span class="text-danger">*</span>
                                    </label>
                                    <select id="desa_id" class="select2 form-control" required disabled>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($list_desa as $data) {
                                                if ($tungsura['desa_id'] == $data['id']) {
                                                    $selected1 = "selected";
                                                } else {
                                                    $selected1 = "";
                                                }
                                        ?>
                                            <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                <?php echo $data['nama']; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Calon Kepala Desa <span class="text-danger">*</span>
                                    </label>
                                    <select name="cakades_id" id="cakades_id" class="select2 form-control" required disabled>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        TPS <span class="text-danger">*</span>
                                    </label>
                                    <select name="tps_id" id="tps_id" class="select2 form-control" required disabled>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 form-radius">
                                <div class="form-group">
                                    <label class="control-label">
                                        Jumlah Suara <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" class="form-control" name="jumlah_suara" value="<?php echo $tungsura['jumlah_suara']; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {

        var desa_id     = '<?php echo $tungsura['desa_id']; ?>';
        var cakades_id  = '<?php echo $tungsura['cakades_id']; ?>';
        var tps_id      = '<?php echo $tungsura['tps_id']; ?>';
        var temp1 = "";
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("rcmadmin/tungsura/get_all_cakades_by_desa"); ?>/' + desa_id,
            success: function (data) {
              $("#cakades_id").empty(); 
              $(data).each(function(i) {
                if ( cakades_id == data[i].id ) {
                  temp1 = "selected";
                } else {
                  temp1 = "";
                }

                $("#cakades_id").append("<option " + temp1 + " value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
              });
            }
        });

        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("rcmadmin/tungsura/get_all_tps_by_desa_on_view"); ?>/' + desa_id,
            success: function (data) {
              $("#id_asset").empty(); 
              $(data).each(function(i) {
                if ( tps_id == data[i].id ) {
                  temp1 = "selected";
                } else {
                  temp1 = "";
                }

                $("#tps_id").append("<option " + temp1 + " value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
              });
            }
        });

        $('#desa_id').change(function () {
            var id = $(this).val();
            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("rcmadmin/tungsura/get_all_cakades_by_desa"); ?>/' + id,
                success: function (data) {
                    $("#cakades_id").empty(); 
                    $("#cakades_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                    $(data).each(function(i) {
                        $("#cakades_id").append("<option value=\"" + data[i].id + "\">" + data[i].nama + " - No. Urut (" + data[i].no_urut + ")</option>");
                    });
                }
            });

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("rcmadmin/tungsura/get_all_tps_by_desa_on_view"); ?>/' + id,
                success: function (data) {
                    $("#tps_id").empty(); 
                    $("#tps_id").append("<option value=''> --- <?php echo $this->lang->line('label_select'); ?> --- </option>");
                    $(data).each(function(i) {
                        $("#tps_id").append("<option value=\"" + data[i].id + "\">" + data[i].nama + "</option>");
                    });
                }
            });
        });
    });
</script>