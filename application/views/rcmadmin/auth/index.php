<section id="wrapper">
    <div class="login-register" style="background-image:url('<?php echo base_url(); ?>images/bg.jpg');">
        <div class="login-box card" style="box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.3)">
            <div style="padding-top: 20px">
                <center><strong><h3>PILKADES SERENTAK 2020</h3></strong></center>
            </div>
            <div class="card-body">
                <form method="post" role="form" id="rent-form" class="form-horizontal form-material">
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username" name="username" id="username"> 
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password" name="password" id="password"> 
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info" type="submit">
                                Login
                            </button>
                        </div>
                    </div>

                    <?php echo alert(); ?>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            @copyright 2020
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>