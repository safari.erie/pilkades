<a href="<?php echo base_url(); ?>rcmadmin/auth" class="btn btn-success" style="background-color: #01c0c8; position: absolute;">
	<?php echo $this->lang->line('button_back'); ?>
</a>
<div class="register-box"  style="margin-top: -35px;">
    <div class="">
        <a href="javascript:void(0)" class="text-center db m-b-40"><img src="<?php echo base_url(); ?>/images/rentist-login.png" alt="Logo" />
            <br/></a>
        <!-- multistep form -->
        <form role="form" action="#" id="msform" data-title="Registration" data-url="/rcmadmin/auth" autocomplete="off">
            <input type="hidden" name="endpoint" value="<?php echo encryptString("signup"); ?>" />
            <input type="hidden" name="registered_on" value="Web" />

            <!-- progressbar -->
            <ul id="eliteregister" style="margin-left: 20%;">
                <li class="active"><?php echo $this->lang->line('label_select_type'); ?></li>
                <li><?php echo $this->lang->line('label_personal_details'); ?></li>
                <li><?php echo $this->lang->line('label_phone_number'); ?></li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title"><b><?php echo $this->lang->line('label_choose_your_account_type'); ?></b></h2>
					
					<div class="col-md-6 text-center">
						<div id="own-box">
							<label>
								<h3>
									<input type="radio" name="id_partner_type" value="1">
									<?php echo $this->lang->line('label_personal'); ?>
								</h3>
							</label>
						</div>
					</div>
					
					<div class="col-md-6 text-center">
						<div id="own-box">
							<label>
								<h3>
									<input type="radio" name="id_partner_type" value="2">
									<?php echo $this->lang->line('label_coorporate'); ?>
								</h3>
							</label>
						</div>
					</div>
					
					<input type="button" name="next" class="next action-button" value="<?php echo $this->lang->line('button_next'); ?>" />
					
            </fieldset>
            <fieldset>
                <h2 class="fs-title"><b><?php echo $this->lang->line('label_personal_details'); ?></b></h2>
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group text-left">
							<label class="control-label"><?php echo $this->lang->line('rental_name'); ?> <span class="text-danger">*</span></label>
							<input class="form-control" type="text" name="rental_name" id="rental_name" placeholder="<?php echo $this->lang->line('rental_name'); ?>" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group text-left">
							<label class="control-label">Email <span class="text-danger">*</span> <span id="divCheckValidateEmail"></span></label>
							<input class="form-control" type="email" name="email" id="email" placeholder="Email" />
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group text-left">
							<label class="control-label">Full Name <span class="text-danger">*</span></label>
							<input class="form-control" type="text" name="full_name" id="full_name" placeholder="Full Name" />
						</div>
					</div>
				</div>
				
				<div class="row" style="margin-bottom: 45px;">
					<div class="col-md-6">
						<div class="form-group text-left">
							<label class="control-label"><?php echo $this->lang->line('password'); ?> <span class="text-danger">*</span></label>
							<input class="form-control" type="password" name="password" id="password" placeholder="<?php echo $this->lang->line('password'); ?>" />
							<div class="hide-show">
						        <span><i class="fa fa-eye"></i></span>
						    </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group text-left">
							<label class="control-label"><?php echo $this->lang->line('repassword'); ?> <span class="text-danger">*</span> <span id="divCheckPasswordMatch"></span></label>
							<input class="form-control" type="password" name="repassword" id="repassword" placeholder="<?php echo $this->lang->line('repassword'); ?>" />
							<div class="hide-show">
						        <span><i class="fa fa-eye"></i></span>
						    </div>
						</div>
					</div>
				</div>
				
                <input type="button" name="previous" class="previous action-button" value="<?php echo $this->lang->line('button_previous'); ?>" />
                <input type="button" name="next" class="next action-button" value="<?php echo $this->lang->line('button_next'); ?>" />
            </fieldset>
            <fieldset>
                <h2 class="fs-title"><b><?php echo $this->lang->line('label_phone_number'); ?> & Referral ID</b></h2>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group text-left">
								<label class="control-label text-left"><?php echo $this->lang->line('phone'); ?></label>
								<input type="tel" class="form-control numeric" name="phone" id="phone" placeholder="No. Handphone" value="+62" style="    padding-left: 52px;">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group text-left">
								<label class="control-label text-left">Referral ID</label>
								<input type="text" class="form-control" name="referrer" id="referrer" placeholder="Referral ID" value="">
							</div>
						</div>
					</div>
                <input type="button" name="previous" class="previous action-button" value="<?php echo $this->lang->line('button_previous'); ?>" />
                <button class="action-button" type="submit">
                    <?php echo $this->lang->line('button_finish'); ?>
                </button>
            </fieldset>
        </form>
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
	.hide-show {
	  	margin: -3.83em 4.66% 0 0.5%;
	    position: relative;
	    z-index: 5;
	}
	.hide-show span {
	    background: #01c0c8;
	    font-size: 1em;
	    padding: 0.5em 0.5em 0.55em 0.5em;
	    float: right;
	    -moz-border-radius: 5px;
	    cursor: pointer;
	    margin-right: -15px;
	    margin-top: -1px;
	    color: white;
	}
</style>

<script type="text/javascript">

	$(function(){
	  	$('.hide-show').show();
	  	$('.hide-show span').addClass('show')
	  
	  	$('.hide-show span').click(function(){
	  		var param = $(this).parent().parent().find('input').attr('id');
	    	if( $(this).hasClass('show') ) {
	      		$(this).html('<i class="fa fa-eye-slash"></i>');
	      		$('#' + param).attr('type','text');
	      		$(this).removeClass('show');
	    	} else {
	       		$(this).html('<i class="fa fa-eye"></i>');
	       		$('#' + param).attr('type','password');
	       		$(this).addClass('show');
	    	}
	  	});
	});
    //password match
    function checkPasswordMatch() {
        var password        = $("#password").val();
        var confirmPassword = $("#repassword").val();

        if ( $.trim(password).length != 0 || $.trim(confirmPassword).length != 0 ) {
        	if ($.trim(password).length < 8) {
        		$("#divCheckPasswordMatch").html("> Password is at least 8 characters long.").css('color', 'red');
        	} else {
	          	if (password != confirmPassword) {
	            	$("#divCheckPasswordMatch").html("> Password does not match!").css('color', 'red');
	          	} else {
	            	$("#divCheckPasswordMatch").html("> Password match.").css('color', 'green');
	          	}
        	}
        }
    }

  	$(document).ready(function () {
    	$("#password, #repassword").keyup(checkPasswordMatch);
  	});

    function validate_email(sEmail) {
    	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        return filter.test(sEmail);
    }

  	$(document).ready(function () {
      	$('#email').keyup(function() {
        	var value = $(this).val();
        	var valid = validate_email(value);

	        if (valid) {
	          $("#divCheckValidateEmail").html("> Email can be used.").css('color', 'green');
	        } else {
	          $("#divCheckValidateEmail").html("> Incorrect email.").css('color', 'red');
	        }
      	});
  	});

</script>