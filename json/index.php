<?php
	function client_ip() {
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
					$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
					$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
					$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
					$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
					$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
					$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
					$ipaddress = 'UNKNOWN';
			return $ipaddress;
	}

	function encryptString($string){
	    $salt = saltString();
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}

	function decryptString($string){
	    $salt = saltString();
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}

	function saltString(){
		return md5(client_ip()."|".$_SERVER["HTTP_USER_AGENT"]);
	}

	$_SERVER["HTTP_REFERER"] = "https://cm.rentist.id";
    if( count(explode("https://cm.rentist.id",$_SERVER["HTTP_REFERER"])) == 2 && isset($_POST["endpoint"]) && !empty($_POST["endpoint"])){
        $_GET['type'] = "partner";
        $_GET['page'] = decryptString($_POST["endpoint"]);
        if( isset($_COOKIE["token_partner_person"]) && !empty($_COOKIE["token_partner_person"]) ){
            $_SERVER['HTTP_TOKEN'] = $_COOKIE["token_partner_person"];
        }else{
            $_SERVER['HTTP_TOKEN'] = "";
        }
        
		if(isset($_POST["key"]) && !empty($_POST["key"])){
			$_GET['data'] = decryptString($_POST["key"]);
		}

		if (decryptString($_POST['action']) == 'PUT') {
			$_SERVER['REQUEST_METHOD'] = "PUT";
			foreach ($_POST as $key => $value) {
				$_PUT[$key] = $value;
			}
		}
		
        require_once("/home/rentist/public_html/api.rentist.id/process-data.php");
    }
?>
